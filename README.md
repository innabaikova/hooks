# Hooks cheatset

Project with hooks examples which can be easily modified and used in real projects

## Hooks rules

1. Hooks can't be used in condition, nested functions or loops because hooks needs order. This order is important due to how React stores values - like in array [read more...](https://reactjs.org/docs/hooks-rules.html#explanation)

2. Hooks can't be used in regular JS functions, only in React component functions or custom hooks. React binds hooks to React component, so it needs to know where hook is binded.

3. You may rely on useMemo as a performance optimization, not as a semantic guarantee. In the future, React may choose to “forget” some previously memoized values and recalculate them on next render, e.g. to free memory for offscreen components. Write your code so that it still works without useMemo — and then add it to optimize performance.