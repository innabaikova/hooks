import React, { createContext } from 'react'
import ColorList from './components/ColorList'
import ContextReader from './components/ContextReader'
import ColorProvider from './providers/ColorProvider'
import SWOnHooksWrapper from './components/SWOnHooksWrapper'
import FocusedInput from './components/FocusedInput'
import Modal from './components/Modal'
import UndoChanges from './components/UndoChanges'
import StateSwitcher from './components/StateSwitcher'
import Toggle from './components/Toggle'
import ObjectChanger from './components/ObjectChanger'
import DebouncedInput from './components/DebouncedInput'
import VirtualizedList from './components/VirtualizedList'
import UsersList from './components/UsersList'
import UserListContainer from './components/UserListContainer'
import GithubUser from './components/GithubUser'
import UserProvider from './providers/UserProvider'
import UserCard from './components/UserCard'
import ContextProvider from './providers/ContextProvider'
import Counter from './components/Counter'
import RandomUser from './components/RandomUser'
import Rating from './components/Rating'
import AverageRating from './components/RatingAverage'
import { ApolloProvider } from '@apollo/client'
import spacexclient from './graphql-clients/space-x-client'
import SpaceXUsers from './components/SpaceXUsers'
import Countries from './components/CountriesSearch'
import UsersTable from './components/UsersTable'
import DebounsedSearch from './components/DebouncedSearch'

const App = () => (
    <div style={{ backgroundColor: 'white', padding: '20px' }}>
        {/* <StateSwitcher />
        <ContextReader />
        <SWOnHooksWrapper />
        <UndoChanges />
        <Modal />
        <Toggle />
        <ObjectChanger /> */}
        {/* <DebouncedInput onChange={(v) => console.log(v)}/> */}
        {/* <VirtualizedList />
        <UserListContainer />*/}
        {/* <UserProvider login={'rocknmuse'}>
            <UserCard />
        </UserProvider> */}
        {/* <ColorProvider>
            <ColorList />
        </ColorProvider> */}
        {/* <ContextProvider value={'Provider value'}>
            <ContextReader />
        </ContextProvider> */}
        {/* <FocusedInput /> */}
        {/* <GithubUser /> */}
        {/* <Counter /> */}
        {/* <RandomUser /> */}
        {/* <Rating name="Keisha Holmes" rate={4} content="Bla bla"/> */}
        {/* <AverageRating ratings={[{rate: 4}, {rate: 5}, {rate: 5}]}/> */}
        {/* <ApolloProvider client={spacexclient}>
            <SpaceXUsers />
        </ApolloProvider> */}
        {/* <Countries /> */}
        {/* <UsersTable /> */}
        <DebounsedSearch />
    </div>
)

export { App }
