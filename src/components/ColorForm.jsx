import React from 'react'
import { useColors } from '../providers/ColorProvider'
import useInput from '../hooks/use-input'

const ColorForm = () => {
    const [titleProps, resetTitle] = useInput('')
    const [colorProps, resetColor] = useInput('#000000')
    const {addColor} = useColors()

    const submit = e => {
        e.preventDefault()
        addColor(titleProps.value, colorProps.value)
        resetTitle()
        resetColor()
    }
    
    return (
        <form onSubmit={submit}>
            <input {...titleProps} type='text' placeholder='Color title' required/>
            <input {...colorProps} type='color' required data-testid='color'/>
            <button type='submit' data-testid='add'>Add</button>
            
        </form>
    )
}

export default ColorForm