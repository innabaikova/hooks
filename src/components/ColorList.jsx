import React from "react"
import { useColors } from "../providers/ColorProvider"
import ColorForm from './ColorForm'
import PureColor from "./PureColor"

const ColorList = () => {
    const { colors } = useColors()

    return (
        <>
            <div style={{display: "flex"}}>
                { colors.map(({id, value, title}) => <PureColor key={id} title={title} value={value}/>)}
            </div>
            <ColorForm />
        </>
    )
}

export default ColorList