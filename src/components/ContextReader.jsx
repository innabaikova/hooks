import React, {useContext} from 'react'
import { MyContext } from '../providers/ContextProvider'


const ContextReader = () => {
    const value = useContext(MyContext)
    return (
    <div>{value}</div>
    )
}

export default ContextReader