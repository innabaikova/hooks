import React, { useReducer } from 'react'

const Counter = () => {
    const [counter, increment] = useReducer(c => ++c, 0)
    return (
        <div>
            <p>Counter value is: {counter}</p>
            <button onClick={increment}>Increment counter</button>
        </div>
    )
}

export default Counter