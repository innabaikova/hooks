import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { debounce } from 'lodash'

const CountriesSearch = () => {
    const [countries, setCountries] = useState([])
    const [search, setSearch] = useState('')

    const handleSearch = debounce(async () => {
        if (search.length) {
            try {
                const result = await axios.get(
                    `https://restcountries.com/v2/name/${search}`
                )
                setCountries(result.data)
            } catch (error) {
                setCountries([])
            }
        } else {
            setCountries([])
        }
    }, 500)

    useEffect(() => {
        return () => handleSearch.cancel()
    }, [])

    return (
        <div>
            <input
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                onKeyUp={handleSearch}
            />
            <ul>
                {countries.map((country) => (
                    <li key={country.alpha3Code}>{country.name}</li>
                ))}
            </ul>
        </div>
    )
}

export default CountriesSearch
