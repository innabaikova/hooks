import React from 'react'
import { useEffect, useState } from 'react'
import useDebounce from '../hooks/use-debounce'

const DebouncedInput = ({ onChange }) => {
    const [val, setVal] = useState('')
    const debouncedValue = useDebounce(val, 500)

    useEffect(() => {
        if (debouncedValue) onChange(debouncedValue)
    }, [debouncedValue, onChange])

    return <input value={val} onChange={(e) => setVal(e.target.value)} />
}

export default DebouncedInput
