import React, { useEffect, useState } from 'react'

const fetchCountries = async (search) => {
    const data = await fetch(
        `https://restcountries.com/v3.1/name/${search}`
    ).then((res) => (res.status === 200 ? res.json() : []))
    const countries = data.map(({ name }) => name.common)
    return countries
}

export default function DebounsedSearch() {
    const [inpVal, setInpVal] = useState('')
    const [value, setValue] = useState('')
    const [countries, setCountries] = useState([])

    useEffect(() => {
        setCountries([])
        let timeout = setTimeout(() => {
            setValue(inpVal)
            inpVal && fetchCountries(inpVal).then(setCountries)
        }, 1000)
        return () => clearTimeout(timeout)
    }, [inpVal])

    return (
        <div className='App'>
            <input
                type='text'
                value={inpVal}
                onChange={(e) => setInpVal(e.target.value)}
            />
            <p>{inpVal}</p>
            <p>{value}</p>
            <ul>
                {countries.map((c) => (
                    <li key={c}>{c}</li>
                ))}
            </ul>
        </div>
    )
}
