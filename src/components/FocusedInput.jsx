import React, {useRef} from "react"

const FocusedInput = () => {
    const ref = useRef()

    return (<div>
        <input ref={ref}/>
        <button onClick={() => ref.current.focus()}>Set focus</button>
    </div>)
}

export default FocusedInput