import React, { useState, useEffect } from 'react'
import client from '../graphql-clients/github-client'
import query from '../queries/find-repos'
import List from './List'
import UserDetails from './UserDetails'

const GithubUser = () => {
    const [login] = useState('rocknmuse')
    const [userData, setUserData] = useState()

    useEffect(() => {
        client
        .request(query, { login })
        .then(({ user }) => user)
        .then(setUserData)
        .catch(console.error)
    }, [login])

    if (!userData) return <p>loading...</p>
    
    return (
        <>
            <UserDetails data={userData} />
            <p>{userData.repositories.totalCount} — repos</p>
            <List
                data={userData.repositories.nodes}
                renderItem={repo => <span>{repo.name}</span>}
            />
        </>
    );
}

export default GithubUser