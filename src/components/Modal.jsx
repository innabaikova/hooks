import React, { useRef, useState } from "react"
import useOnClickOutside from "../hooks/use-on-click-outside"

const Modal = () => {
    const ref = useRef()
    const [isModalOpen, setModalOpen] = useState(true)
    useOnClickOutside(ref, () => setModalOpen(false))

    return (isModalOpen && <div ref={ref}>I'm modal. Click outside to hide me</div>)
}

export default Modal