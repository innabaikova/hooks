import React, {useEffect, useState} from 'react'

const Notification = () => {
    const [isVisible, setVisible] = useState(true)

    useEffect(() => {
        const timeout = setTimeout(() => setVisible(false), 2500)
        return () => clearTimeout(timeout) //avoiding memory leak when component doesn't exist
    },[])

    return (<>{isVisible && <div>Notification message</div>}</>)
}

export default Notification