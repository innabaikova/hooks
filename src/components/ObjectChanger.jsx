import React from "react";
import useSetProperty from "../hooks/use-set-property";

const ObjectChanger = () => {
    const [state, setProperties] = useSetProperty({name: 'Inna', age: 29})

    return(
        <>
         <input type="text" onChange={e => setProperties({name: e.target.value})}/>
         <input type="number" onChange={e => setProperties({age: e.target.value})}/>
         { Object.keys(state).map(key => <p key={key}>{key}: {state[key]}</p>) }
        </>
    )
}

export default ObjectChanger