import React, {useCallback} from 'react'
import useRequest from '../hooks/use-request'

const getPerson = (id) => {
    return fetch(`https://swapi.dev/api/people/${id}`)
        .then(res => res.json())
        .then(data => data)
}

const PersonInfo = ({id}) => {
    // Used to memorize callback link
    const request = useCallback(() => getPerson(id), [id])
    // without useCallback request is always a new callback link which will trigger useEffect inside useRequest
    const data = useRequest(request)

    return (<>{data && <div>{id} - {data.name}</div>}</>)
}

export default PersonInfo