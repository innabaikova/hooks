import React, {useEffect, useState} from 'react'

const PlanetInfo = ({id}) => {
    const [name, setName] = useState(null)

    useEffect(() => {
        let canceled = false
        setName(null)
        fetch(`https://swapi.dev/api/planets/${id}`)
        .then(res => res.json())
        .then(data => !canceled && setName(data.name))
        return () => canceled = true
    }, [id])

    // or with custom hook from use-custom
    //const name = useNameById('planets', id)

    return (<>{name && <div>{id} - {name}</div>}</>)
}

export default PlanetInfo