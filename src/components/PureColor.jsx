import React, { memo } from 'react'

const PureColor = memo(({ title, value }) => {
    return (
        <div
            style={{
                height: 50,
                width: 50,
                backgroundColor: value,
                color: 'white'
            }}
            data-testid='colorblock'>
            {title}
        </div>
    )
})

export default PureColor
