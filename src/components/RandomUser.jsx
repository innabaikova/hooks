import React, { useEffect, useReducer, useState } from 'react'

const RandomUser = () => {
    const [data, setData] = useState([])
    const [isLoading, toggleLoading] = useReducer((loading) => !loading, false)
    const [error, setError] = useState(null)
    const [page, incrementPage] = useReducer(p => p+1, 1)

    useEffect(() => {
        let canceled = false
        const fetchData = async () => {
            toggleLoading()
            try {
                const result = await fetch(
                    `https://randomuser.me/api?page=${page}&results=10`
                )
                if (!result.ok) {
                    throw Error(`Server responded with error ${result.status}`)
                }
                const {results} = await result.json()

                if (!canceled) {
                    setData(data => [...data, ...results])
                }
            } catch (error) {
                setError(error.message)
            } finally {
                toggleLoading()
            }
        }
        fetchData()
        return () => (canceled = true)
    }, [page])

    if (isLoading) return <div>Loading...</div>

    return (
        <>
            {data ? (
                data.map((user) => {
                    return (
                        <div key={user.login.uuid}>
                            <img
                                src={user.picture.large}
                                alt={user.name.first}
                            />
                            <div>
                                <div>
                                    {user.name.first} {user.name.last}
                                </div>
                                <div>{user.email}</div>
                            </div>
                        </div>
                    )
                })
            ) : (
                <p>Something went wrong: {error}</p>
            )}
            {data && <button onClick={incrementPage}>Load more</button>}
        </>
    )
}

export default RandomUser
