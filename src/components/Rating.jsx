import React from 'react'

const Rating = ({ name, rate, content }) => {
    return (
        <div className='ratings__item'>
            {[...Array(5)].map((_, idx) => (
                <span key={idx}>{idx < rate ? '★' : '☆'}</span>
            ))}
            <h3>{name}</h3>
            <p>{content}</p>
        </div>
    )
}

export default Rating
