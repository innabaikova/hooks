import React, {useState, useEffect} from "react";
import Rating from "./Rating";

const AverageRating = ({ ratings }) => {
    const [average, setAverage] = useState(0);

    useEffect(() => {
        setAverage(ratings.reduce((acc, curr) => acc + curr.rate, 0) / ratings.length);
    }, [ratings]);
    return <div className="ratings__average"><Rating rate={average}></Rating></div>;
};

export default AverageRating