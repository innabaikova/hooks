import React, {useEffect, useState} from 'react'
import PersonInfo from './PersonInfo'
import { StarshipInfo } from './StarshipInfo'
import PlanetInfo from './PlanetInfo'
import Notification from './Notification'
import { SpeciesInfo } from './SpeciesInfo'

const SWOnHooksWrapper = () => {
    const [counter, setCounter] = useState(1)
    const [isVisible, setVisible] = useState(true)
    const buttonText = isVisible ? 'hide' : 'show'

    return (
        <>
            {isVisible && <div>
                <HookCounter value={counter}></HookCounter>
                <button onClick={() => setCounter((c) => c + 1)}>+</button>
                <Notification />
                <PlanetInfo id={counter}/>
                <StarshipInfo id={counter + 1}/>
                <PersonInfo id={counter}/>
                <SpeciesInfo id={counter}/>
            </div>}
            <button onClick={() => setVisible((v) => !v)}>{buttonText}</button>
        </>
    )
}

const HookCounter = ({value}) => {
    useEffect(() => {
        console.log('component did mount')
        return () => console.log('component did unmount')
    }, [])

    useEffect(() => {
        console.log('component did update')
    }, [value])
    
    return (<div>Current value: {value}</div>)
}

export default SWOnHooksWrapper