import { useMutation, useQuery } from '@apollo/client'
import React from 'react'
import GET_USERS from '../queries/space-x-users'
import SET_ROCKET from '../mutations/space-x-user'

const SpaceXUsers = () => {
    const { data, loading, error } = useQuery(GET_USERS)
    const [setRocket] = useMutation(SET_ROCKET)

    if (loading) return <p>Loading...</p>
    if (error) return <p>{error.message}</p>

    return (
        <div className='spacex__users'>
            {data.users.map((user) => (
                <div key={user.id}>
                    <p>{user.name}</p>
                    {user.rocket && <p>{user.rocket}</p>}
                    <button onClick={() => setRocket({ variables: {id: user.id}})}>Set Rocket</button>
                </div>
            ))}
        </div>
    )
}

export default SpaceXUsers
