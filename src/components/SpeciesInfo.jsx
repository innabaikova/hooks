import React from 'react'
import useDataStateById from '../hooks/use-data-state-by-id'

const SpeciesInfo = ({id}) => {
    const {data} = useDataStateById('species', id)

    return (<>{data && <div>{id} - {data.name}</div>}</>)
}

export {SpeciesInfo, useDataStateById}