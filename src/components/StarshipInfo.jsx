import React from 'react'
import useNameById from '../hooks/use-name-by-id'

const StarshipInfo = ({id}) => {
    const name = useNameById('starships', id) //can be reused in Planet Info from use-effect-fetch

    return (<>{name && <div>{id} - {name}</div>}</>)
}

export {StarshipInfo, useNameById}