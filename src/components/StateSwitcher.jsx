import React, {useState} from 'react'

const StateSwitcher = () => {
    const [color, setColor] = useState()
    const [font, setFont] = useState(14)

    return (
        <div style={{padding: '10px', backgroundColor: color, fontSize: `${font}px`}}>
            Hi Inna <br/>
            <button onClick={() => setColor('white')}>White</button>
            <button onClick={() => setColor('gray')}>Dark</button>
            <button onClick={() => setFont((prev) => prev + 1)}>+</button>
        </div>
    )
}

export default StateSwitcher