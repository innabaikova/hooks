import React, { useReducer } from "react";

const Toggle = () => {
    const [value, toggle] = useReducer(checked => !checked, false)

    return (
        <div>
            <input type="checkbox" checked={value} onChange={toggle} />
            {value ? 'checked' : 'not checked'}
        </div>
    )
}

export default Toggle