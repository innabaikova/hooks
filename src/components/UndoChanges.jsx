import React, { useState } from "react"
import usePrevious from "../hooks/use-previous"

const UndoChanges = () => {
    const [counter, setCounter] = useState(1)
    const prevCounter = usePrevious(counter)

    return (<>
        <div>Counter value: {counter}</div>
        <div>Previous value: {prevCounter}</div>
        <button onClick={() => setCounter(c => c+1)}>+</button>
        <button onClick={() => setCounter(prevCounter)}>Undo</button>
    </>)
}

export default UndoChanges