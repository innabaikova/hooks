import React from 'react'
import { useUser } from '../providers/UserProvider'

const UserCard = () => {
    const user = useUser()

    return (
        <div className='githubUser'>
            {user && (
                <>
                    <img
                        src={user.avatarUrl}
                        alt={user.login}
                        style={{ width: 200 }}
                    />
                    <div>
                        <h1>{user.login}</h1>
                        {user.name && <p>{user.name}</p>}
                        {user.location && <p>{user.location}</p>}
                    </div>
                </>
            )}
        </div>
    )
}

export default UserCard
