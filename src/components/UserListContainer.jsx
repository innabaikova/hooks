import React from 'react'
import { useState } from 'react'
import UsersList from './UsersList'

const UserListContainer = () => {
    const [visible, setVisible] = useState(true)
    
    return(
        <>
            <button onClick={() => setVisible(false)}>Hide List</button>
            {visible && <UsersList />}
        </>
    )
}

export default UserListContainer