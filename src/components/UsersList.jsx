import React from 'react'
import useFetch from '../hooks/use-fetch'
import { FixedSizeList as List } from 'react-window'

const UsersList = () => {
    const {isLoading, data, error} = useFetch('https://api.github.com/users')

    const renderUser = ({index, style}) => {
        const {avatar_url, login, url} = data[index]
        const rowStyle = {...style, display: 'flex'}

        return (
            <div style={rowStyle}>
                <img src={avatar_url} alt={login} width={50} style={{marginRight: '15px'}}/>
                <p>
                    {login} - {url}
                </p>
            </div>
        )
    }
 
    if (isLoading) return(<h1>loading...</h1>)
    if (error) return (<pre>{JSON.stringify(error, null, 2)}</pre>)

    return(
        <List
            height={window.innerHeight - 40}
            width={window.innerWidth - 40}
            itemCount={data.length}
            itemSize={50}
        >
            {renderUser}
        </List>
    )
}

export default UsersList