import React, { useEffect, useState } from 'react'
import axios from 'axios'

// https://randomuser.me/api&results=10

const flattenLocation = (location) => {
    const { city, state, street } = location
    return `${city}, ${state} ${street.name} ${street.number}`
}

const sortings = {
    asc: (a, b) => (a > b ? 1 : -1),
    desc: (a, b) => (a > b ? -1 : 1)
}

const fetchUsers = () => {
    return axios
        .get('https://randomuser.me/api', { params: { results: 10 } })
        .then(({ data }) => {
            return data.results.map((user) => {
                return {
                    ...user,
                    id: user.login.uuid,
                    picture: user.picture.thumbnail,
                    login: user.login.username,
                    name: `${user.name.first} ${user.name.last}`,
                    age: user.dob.age,
                    location: flattenLocation(user.location),
                    dob: user.dob.date,
                    registered: user.registered.date
                }
            })
        })
}

const UsersTable = () => {
    const [users, setUsers] = useState([])
    const [sorting, setSorting] = useState(null)
    const [search, setSearch] = useState('')
    const [debounced, setDebounced] = useState('')

    useEffect(() => {
        fetchUsers().then((users) => {
            setUsers(users)
        })
    }, [])

    useEffect(() => {
        const timeout = setTimeout(() => {
            setDebounced(search)
        }, 1000)

        return () => clearTimeout(timeout)
    }, [search])

    const saveSorting = (field) => {
        setSorting((sorting) => {
            if (sorting?.field === field) {
                if (sorting.direction === 'desc') return null
                return { field, direction: 'desc' }
            }

            return {
                field,
                direction: 'asc'
            }
        })
    }

    const filterUsers = (users) => {
        let filteredUsers = users
        if (sorting) {
            const { field, direction } = sorting

            filteredUsers = [...users].sort((a, b) => {
                return sortings[direction](a[field], b[field])
            })
        }
        return filteredUsers.filter((user) =>
            Object.values(user).some((value) =>
                value.toString().toLowerCase().includes(debounced.toLowerCase())
            )
        )
    }

    return (
        <>
            <input
                type='text'
                placeholder='Search'
                value={search}
                onChange={(e) => setSearch(e.target.value)}
            />
            {users.length > 0 && (
                <table>
                    <thead>
                        <tr>
                            {Object.keys(users[0]).map((key) => {
                                return (
                                    <th
                                        key={key}
                                        onClick={() => saveSorting(key)}>
                                        {key}
                                    </th>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {filterUsers(users).map((user) => {
                            return (
                                <tr key={user.id}>
                                    {Object.keys(user).map((key) => {
                                        return <td key={key}>{user[key]}</td>
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            )}
        </>
    )
}

export default UsersTable
