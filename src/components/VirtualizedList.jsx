import React from 'react'
import { FixedSizeList as List } from 'react-window'
import bigList from '../utils/big-list'

const VirtualizedList = () => {
    const renderItem = ({index, style}) => {
        const {avatar, name, email} = bigList[index]
        const rowStyle = {...style, display: 'flex'}

        return (
            <div style={rowStyle}>
                <img src={avatar} alt={name} width={50}/>
                <p>
                    {name} - {email}
                </p>
            </div>
        )
    }

    return (
        <List 
            height={window.innerHeight - 40}
            width={window.innerWidth - 40}
            itemCount={bigList.length}
            itemSize={50}
        >
            {renderItem}
        </List>
    )
}

export default VirtualizedList