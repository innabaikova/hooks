import { render, screen, fireEvent, cleanup } from '@testing-library/react'
import React from 'react'
import ColorProvider from '../../providers/ColorProvider'
import ColorList from '../ColorList'

describe('Colors component', () => {
    beforeEach(() => {
        render(
            <ColorProvider>
                <ColorList />
            </ColorProvider>
        )
    })

    afterEach(cleanup)

    it('should display default color list', () => {
        const colorBlocks = screen.getAllByTestId('colorblock')
        expect(colorBlocks.length).toBe(2)
    })

    it('should be able to add colors to list', () => {
        const values = {
            title: 'blue',
            color: '#0000ff',
            colorRGB: 'rgb(0, 0, 255)'
        }
        const titleInput = screen.getByPlaceholderText('Color title')
        fireEvent.change(titleInput, { target: { value: values.title } })

        const colorInput = screen.getByTestId('color')
        fireEvent.change(colorInput, { target: { value: values.color } })

        const button = screen.getByTestId('add')
        fireEvent.click(button)

        const colorBlocks = screen.getAllByTestId('colorblock')
        const newBlock = colorBlocks[colorBlocks.length-1]
        expect(newBlock.textContent).toBe(values.title)
        expect(newBlock.style.backgroundColor).toBe(values.colorRGB)
        expect(colorBlocks.length).toBe(3)
    })
})
