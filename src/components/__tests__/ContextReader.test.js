import { render } from '@testing-library/react'
import React from 'react'
import ContextProvider from '../../providers/ContextProvider'
import ContextReader from '../ContextReader'

it('ContextReader should output context value in a div', () => {
    const value = 'Blablabla'
    const { getByText } = render(
        <ContextProvider value={value}>
            <ContextReader />
        </ContextProvider>
    )
    const element = getByText(value)
    expect(element).toBeTruthy()
})
