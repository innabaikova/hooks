import React from 'react'
import { render } from '@testing-library/react'
import Counter from '../Counter'
import userEvent from '@testing-library/user-event'

describe('Counter component', () => {
    it('should display initial counter value', () => {
        const { container } = render(<Counter />)
        const displayed = container.querySelector('p')
        expect(displayed.innerHTML).toBe('Counter value is: 0')
    })

    it('should increment displayed value when increment button has been pushed', () => {
        const { container, getByRole } = render(<Counter />)
        const button = getByRole('button')
        userEvent.click(button)
        const displayed = container.querySelector('p')
        expect(displayed.innerHTML).toBe('Counter value is: 1')
    })
})


