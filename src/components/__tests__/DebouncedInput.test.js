import { fireEvent, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import React from 'react'
import DebouncedInput from '../DebouncedInput'

it('DebouncedIput should fire change callback once in 1000ms after user stops typing', () => {
    jest.useFakeTimers()

    const callback = jest.fn()
    const { getByRole } = render(<DebouncedInput onChange={callback} />)
    const input = getByRole('textbox')
    const value = 'askdjhkjh'
    for(let i=0; i < value.length; i++) {
        userEvent.type(input, i === value.length - 1 ? value : value[i])
    }

    jest.runAllTimers()
    expect(callback).toBeCalledTimes(1)
    expect(callback).toBeCalledWith(value)
})
