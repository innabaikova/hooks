import React from 'react'
import { render } from '@testing-library/react/pure'
import FocusedInput from '../FocusedInput'
import userEvent from '@testing-library/user-event'

describe('FocusedInput component', () => {
    let input
    let button

    beforeAll(() => {
        const { getByRole } = render(<FocusedInput />)
        input = getByRole('textbox')
        button = getByRole('button')
    })

    it('should render an input and button', () => {
        expect(input).toBeTruthy()
        expect(button).toBeTruthy()
    })

    it('should set focus on input when button has been clicked', () => {
        userEvent.click(button)
        expect(document.activeElement).toBe(input)
    })
})
