import React from 'react'
import { render, screen } from '@testing-library/react/pure'
import GithubUser from '../GithubUser'
import client from '../../graphql-clients/github-client'
import { cleanup } from '@testing-library/react/dist/pure'

const mockData = {
    user: {
        login: 'rocknmuse',
        name: 'Inna Baikova',
        location: null,
        avatar_url:
            'https://someavatarurl.com',
        repositories: {
            totalCount: 1,
            nodes: [
                {
                    name: 'rocknmuse-recipe-selection-react-test'
                }
            ]
        }
    }
}

jest.mock('../../graphql-clients/github-client.js', () => {
    const mockClient = { request: jest.fn() }
    return mockClient
})

describe('GitHubUser component when it recieves data', () => {
    beforeAll(() => {
        client.request.mockResolvedValueOnce(mockData)
        render(<GithubUser />)
    })

    afterAll(cleanup)
    it('should render user image', () => {
        expect(screen.getByRole('img')).toHaveAttribute('src', mockData.user.avatar_url)
    })

    it('should render user login', () => {
        expect(screen.getByText(mockData.user.login)).toBeTruthy()
    })

    it('should render user name', () => {
        expect(screen.getByText(mockData.user.name)).toBeTruthy()
    })

    it('should render list of repos', () => {
        expect(screen.getAllByRole('listitem').length).toBe(mockData.user.repositories.totalCount)
    })
})
