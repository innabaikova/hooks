import {useEffect, useState, useMemo} from 'react'

const useDataStateById = (resource, id) => {
    // useMemo saves function call result, useCallback saves function
    const initialDataState = useMemo(() => ({
        data: null,
        loading: true,
        error: false
    }), [])

    const [dataState, setDataState] = useState(initialDataState)

    useEffect(() => {
        let canceled = false
        // without useMemo setDataState will trigger render and creating a new initialDataState
        setDataState(initialDataState)
        fetch(`https://swapi.dev/api/${resource}/${id}`)
        .then(res => res.json())
        .then(data => !canceled && setDataState({
            data,
            loading: false,
            error: false
        }))
        .catch(e => ({
            data: null,
            loading: false,
            error: 'Something went wrong'
        }))
        return () => canceled = true
        // every change to initial dataState triggers useEffect
    }, [id, resource, initialDataState])

    return dataState
}

export default useDataStateById