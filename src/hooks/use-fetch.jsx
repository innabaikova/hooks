import { useEffect, useState } from 'react'

const useFetch = (url) => {
    const [data, setData] = useState()
    const [isLoading, setIsLoading] = useState(true)
    const [error, setError] = useState()

    useEffect(() => {
        let canceled = false
        const fetchData = async() => {
            try {
                const res = await fetch(url)
                const json = await res.json()

                if (!canceled) {
                    setData(json)
                    setIsLoading(false)
                }
            } catch (e) { !canceled && setError(e) }
        }
        
        fetchData()
        return () => canceled = true
    }, [url])

    return { data, error, isLoading }
}

export default useFetch
