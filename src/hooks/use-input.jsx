import {useRef, useState} from 'react'

const useInput = (initialValue) => {
    const [value, setValue] = useState(initialValue)
    const ref = useRef()

    const validate = (v) => {
        ref.current.style.borderColor = v.length < 3 ? 'red' : 'green'
        setValue(v)
    }

    return [
        {value, onChange: e => validate(e.target.value), ref},
        () => setValue(initialValue)
    ]
}

export default useInput