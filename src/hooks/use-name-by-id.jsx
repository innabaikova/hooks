import {useEffect, useState} from 'react'

const useNameById = (resource, id) => {
    const [name, setName] = useState(null)

    useEffect(() => {
        let canceled = false
        setName(null)
        fetch(`https://swapi.dev/api/${resource}/${id}`)
        .then(res => res.json())
        .then(data => !canceled && setName(data.name))
        return () => canceled = true
    }, [id, resource])

    return name
}

export default useNameById