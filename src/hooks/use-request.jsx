import {useEffect, useState} from 'react'

const useRequest = (request) => {
    const [dataSet, setDataSet] = useState(null)

    useEffect(() => {
        let canceled = false
        setDataSet(null)
        request().then(data => !canceled && setDataSet(data))         
        return () => canceled = true
    }, [request])

    return dataSet
}

export default useRequest