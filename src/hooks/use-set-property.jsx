import { useReducer } from "react";

const useSetProperty = (initObj) => useReducer((obj, newDetails) => ({...obj, ...newDetails}), initObj)

export default useSetProperty