import { gql } from '@apollo/client'

const mutation = gql`
    mutation updateUsers($id: uuid!){
        update_users(
            _set: { rocket: "RockItRocket" }
            where: { id: { _eq: $id } }
        ) {
            affected_rows
            returning {
                id
                name
                rocket
            }
        }
    }
`

export default mutation