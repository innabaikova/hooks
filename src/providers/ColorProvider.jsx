import React, { createContext, useContext, useState } from 'react'

const colorsData = [
    {
        id: 0,
        title: 'black',
        value: '#000000'
    },
    {
        id: 1,
        title: 'red',
        value: '#ff0000'
    }
]
const ColorContext = createContext()

const ColorProvider = ({children}) => {
    const [colors, setColors] = useState(colorsData)

    const addColor = (title, value) => setColors(
        [...colors, { id: colors.length + 1, title, value}]
    )

    const removeColor = id => colors.filter(color => color.id !== id)

    return (
        <ColorContext.Provider value={{colors, setColors, addColor, removeColor}}>
            {children}
        </ColorContext.Provider>
    )
}

export const useColors = () => useContext(ColorContext)
export default ColorProvider