import React, { createContext } from 'react'

export const MyContext = createContext()
const ContextProvider = ({value, children}) => {
    
    return (
        <MyContext.Provider value={value}>
            {children}
        </MyContext.Provider>
    )
}

export default ContextProvider