import React from 'react'
import { useContext, useState, useEffect } from 'react'
import { createContext } from 'react'
import client from '../graphql-clients/github-client'
import query from '../queries/github-user'

const UserContext = createContext()
const UserProvider = ({ login, children }) => {
    const [userData, setUserData] = useState()

    useEffect(() => {
        async function fetchUser() {
            try {
                const data = await client
                    .request(query, { login })
                    .then(({ user }) => user)
                setUserData(data)
            } catch (e) {
                console.error(e)
            }
        }
        fetchUser()
    }, [login])

    if (!userData) return null

    return (
        <UserContext.Provider value={userData}>{children}</UserContext.Provider>
    )
}

export const useUser = () => useContext(UserContext)
export default UserProvider
