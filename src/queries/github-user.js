import {gql} from 'graphql-request'

const query = gql`
  query getUser($login: String!) {
    user(login: $login) {
      id
      login
      name
      location
      avatarUrl
      isViewer
    }
  }
`

export default query