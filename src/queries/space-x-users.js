import { gql } from "@apollo/client"

const query = gql`{
    users(limit: 10, order_by: [{id: desc}]) {
        id
        name
        rocket
    }
}
`

export default query